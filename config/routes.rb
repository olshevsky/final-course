Rails.application.routes.draw do
  namespace :teacher do
  namespace :api do
    get 'api/index'
    end
  end

  devise_for :users
  root 'site/home#index'

  namespace :teacher do
    get '/' => 'base#index'

    namespace :api, defaults: { format: :json } do
      get '/' => 'api#index'

      scope '/profile' do
        get '/' => 'profile#show'
        patch '/' => 'profile#update'
      end

      scope '/group' do
        get '/' => 'group#all'
        get '/:id' => 'group#show'
        # get '/:id/students' => 'group#students'
        # get '/:id/lessons' => 'group#lesson'
      end

      scope '/homework' do
        get '/' => 'homework#all'
        get '/:id' => 'homework#show'
        get '/group/:id' => 'homework#group'
        delete '/delete/:id' => 'homework#delete'
        patch '/:id' => 'homework#update'
        post '/new' => 'homework#create'
      end

      scope '/lesson' do
        get '/' => 'timetable#index'
        get '/:id' => 'timetable#show'
      end

      scope '/salary' do
        get '/' => 'accounting#all'
        get '/:id' => 'accounting#show'
        get '/:id/report' => 'accounting#report'
      end

      scope 'report' do
        get '/' => 'report#all'
        get '/:id' => 'report#show'
      end
    end
  end

  namespace 'student' do

    get '/' => 'base#index'

    namespace :api, defaults: { format: :json } do
      get '/' => 'api#index'

      scope '/lesson' do
        get '/' => 'timetable#index'
      end

      scope '/group' do
        get '/' => 'group#all'
        get '/:id' => 'group#show'
      end

      scope '/homework' do
        get '/' => 'homework#index'
        get '/:id' => 'homework#show'
        get '/group/:id' => 'homework#group'
      end

      scope '/pays' do
        get '/' => 'accounting#index'
        get '/:id' => 'accounting#show'
      end

      scope '/profile' do
        get '/' => 'profile#show'
        patch '/' => 'profile#update'
      end

      scope '/report' do
        get '/' => 'report#all'
        get '/:id' => 'report#get'
      end

    end
  end

  namespace 'admin' do

    get '/' => 'base#index'

    namespace :api, defaults: { format: :json } do
      get '/' => 'api#index'
      get '/profile' => 'profile#index'
      get '/calendar' => 'api#calendar'

      scope '/data' do
        get '/lesson' => 'data#lesson'
      end

      scope '/homework' do
        get '/' => 'homework#index'
        get '/:id' => 'homework#show'
        get '/group/:id' => 'homework#group'
      end

      resources :pages
      resources :skills
      resources :users
      resources :courses, controller: "course" do
        member do
          get '/archive', to: 'course#archive', as: 'archive'
          get '/activate', to: 'course#activate', as: 'activate'
          get '/reject', to: 'course#reject', as: 'reject'
        end
      end
      resources :languages
      resources :lessons

      get '/group/form_data', to: 'group#form_data', as: 'group_form_data'
      get '/teacher/form_data', to: 'teacher#form_data', as: 'teacher_form_data'
      resources :groups, controller: "group" do
        resources :distributions, only: [:new, :create, :destroy], :controller => 'group_distributions'
        member do
          get '/archive', to: 'group#archive', as: 'archive'
          get '/activate', to: 'group#activate', as: 'activate'
        end
      end
      resources :students do
        resources :distributions, only: [:new, :create, :destroy]
        resources :pays, only: [:create, :destroy]
      end
      resources :teachers, :controller => "teacher" do
        resources :skills, only: [:create, :destroy]
        resources :salaries, only: [:create, :destroy]
        member do
          get '/archive', to: 'teacher#archive', as: 'archive'
          get '/activate', to: 'teacher#activate', as: 'activate'
        end
      end

      resources :events, controller: "event" do
        member do
          get '/archive', to: 'event#archive', as: 'archive'
          get '/activate', to: 'event#activate', as: 'activate'
          get '/reject', to: 'event#reject', as: 'reject'
        end
      end
    end
  end
end
