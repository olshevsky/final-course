class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.belongs_to :teacher
      t.belongs_to :group
      t.string :day
      t.time :startTime
      t.time :endTime
      t.belongs_to :group
      t.string :classRoom
      t.timestamps
    end
  end
end
