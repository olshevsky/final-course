class AddTwoColumnsToSalariesTypeAndNumOfLessons < ActiveRecord::Migration
  def change
    add_column :salaries, :type, :string
    add_column :salaries, :num_lessons, :integer
  end
end
