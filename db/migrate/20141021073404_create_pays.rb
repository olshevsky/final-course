class CreatePays < ActiveRecord::Migration
  def change
    create_table :pays do |t|
      t.float :sum
      t.belongs_to :student
      t.timestamps
    end
  end
end
