class CreateLoads < ActiveRecord::Migration
  def change
    create_table :loads do |t|
      t.float :rate
      t.belongs_to :group
      t.belongs_to :teacher
      t.timestamps
    end
  end
end
