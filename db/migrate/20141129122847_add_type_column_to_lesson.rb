class AddTypeColumnToLesson < ActiveRecord::Migration
  def change
    add_column :lessons, :type, :string, :default => Lesson::PLANED_LESSON
  end
end
