class CreateSalaries < ActiveRecord::Migration
  def change
    create_table :salaries do |t|
      t.float :sum
      t.belongs_to :teacher
      t.timestamps
    end
  end
end
