class SetDefaultValueForDistribution < ActiveRecord::Migration
  def change
    change_column_default :distributions, :status, Distribution::STATUS_ACTIVE
  end
end
