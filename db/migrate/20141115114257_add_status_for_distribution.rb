class AddStatusForDistribution < ActiveRecord::Migration
  def change
    add_column :distributions, :status, :string
  end
end
