class AddLoadColumnToSalaries < ActiveRecord::Migration
  def change
    add_column :salaries, :load, :belongs_to
  end
end
