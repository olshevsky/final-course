class AddDayNumToLesson < ActiveRecord::Migration
  def change
    add_column :lessons, :day_num, :integer
  end
end
