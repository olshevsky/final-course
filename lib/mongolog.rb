class Mongolog
  public
  def add_notice data
    write Log::NOTICE, data
  end

  def add_debug data
    write Log::DEBUG, data
  end

  def add_alert data
    write Log::ALERT, data
  end

  private
  def write(type, data)
    @date_time = DateTime.now
    Log.create({
        :date => @date_time.strftime("%A, %d %b %Y %l:%M %p"),
        :metadata => data,
        :type => type
    })
  end
end