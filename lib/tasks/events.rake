namespace :events do
  desc 'Generate events for lessons'
  task :create => :environment do
    @lessons = Lesson.where(day: Time.now.strftime("%A").downcase)
    @lessons.each do |lesson|
      Event.create({
          start: Time.zone.local_to_utc(lesson.startTime),
          end: Time.zone.local_to_utc(lesson.endTime),
          meta: Group::META_NAME,
          meta_id: lesson.group_id,
          type: Event::LESSON_TYPE,
          event_class: Event::CLASS_ACTIVE,
          status: Event::STATUS_ACTIVE,
          date: Date.today,
          title: Event::LESSON_MESSAGE + lesson.group.title
                   })
    end
  end

  desc 'Close events'
  task :close => :environment do
    @finished_events = Event.where(:date => Time.now.in_time_zone, :type => Event::LESSON_TYPE)
    @finished_events.each do |event|
      if event.status == Event::STATUS_ACTIVE
        event.status = Event::STATUS_FINISHED_SUCCESS
        event.class = Event::CLASS_FINISHED_SUCCESS
      else if event.status == Event::STATUS_CANCELED
          event.status = Event::STATUS_REJECTED
          event.class = Event::CLASS_REJECTED
        end
      end
      event.save
    end
  end

  desc "TODO"
  task :notify => :environment do

  end

end
