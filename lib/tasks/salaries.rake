namespace :salaries do
  desc 'Create salaries'
  task :create => :environment do
    teachers = Teacher.all
    teachers.each do |teacher|
      teacher.loads.each do |load|
        events_count = Event.where(
            :date.gte => Time.now.utc.beginning_of_month,
            :date.lte => Time.now.utc.end_of_month,
            :type => Event::LESSON_TYPE,
            :meta => Group::META_NAME,
            :meta_id => load.group_id,
            :status => Event::STATUS_FINISHED_SUCCESS
        ).count
        salary = Salary.create({
            :teacher => teacher,
            :load => load,
            :sum => load.rate * events_count,
            :type => Salary::SALARY_TYPE,
            :num_lessons => events_count
                               })
      end
    end
  end
end
