class Language < ActiveRecord::Base
  has_many :skills
  has_many :teachers, through: :skills
  has_many :groups
end
