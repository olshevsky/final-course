class Admin < ActiveRecord::Base

  META_NAME = "Admin"

  has_one :user, as: :meta, dependent: :destroy
  accepts_nested_attributes_for :user
end
