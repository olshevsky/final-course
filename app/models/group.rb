class Group < ActiveRecord::Base

  STATUS_FINISHED = "finished"
  STATUS_ACTIVE = "active"
  STATUS_BLOCKED = "blocked"
  STATUS_ARCHIVE = "archive"

  CLASS_FINISHED = "label-success"
  CLASS_ACTIVE = "label-primary"
  CLASS_BLOCKED = "label-danger"
  CLASS_ARCHIVE = "label-warning"

  META_NAME = "Group"

  belongs_to :teacher
  belongs_to :course
  belongs_to :language
  has_many :homeworks, :dependent => :destroy
  has_many :lessons, :dependent => :destroy
  has_many :distributions, :dependent => :destroy
  has_many :students, through: :distributions
  belongs_to :language
end
