class Course < ActiveRecord::Base

  STATUS_ACTIVE = 'active'
  STATUS_FINISHED = 'finished'
  STATUS_REJECTED = 'rejected'
  STATUS_ARCHIVE = 'archive'

  has_many :groups
  has_many :lessons
  has_many :teachers, through: :groups
end
