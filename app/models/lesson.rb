class Lesson < ActiveRecord::Base

  PLANED_LESSON = 'planed'
  REPLACED_LESSON = 'replace'

  belongs_to :course
  belongs_to :group
  belongs_to :teacher
end
