class Event
  LESSON_MESSAGE = 'Lesson in '
  LESSON_DESCRIPTION = 'Planned lesson'

  STATUS_CANCELED = 'canceled'
  STATUS_ACTIVE = 'active'
  STATUS_ARCHIVE = 'archive'
  STATUS_FINISHED_SUCCESS = 'finished_success'
  STATUS_REJECTED = 'rejected'

  LESSON_TYPE = 'lesson'
  EVENT_TYPE = 'event'

  CLASS_ACTIVE = 'event-active'
  CLASS_ARCHIVE = 'event-archive'
  CLASS_FINISHED_SUCCESS = 'event-finished'
  CLASS_CANCELED = 'event-canceled'
  CLASS_EVENT = 'personal-event'
  CLASS_REJECTED = 'event-canceled'

  include Mongoid::Document
  field :start, type: DateTime
  field :status, type: String
  field :type, type: String
  field :meta, type: String
  field :meta_id, type: Integer
  field :end, type: DateTime
  field :title, type: String
  field :teacher, type: Integer
  field :event_class, type: String
  field :description, type: String
  field :date, type: Date
end
