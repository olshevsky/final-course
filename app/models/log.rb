class Log

  DEBUG = 'debug'
  ALERT = 'alert'
  NOTICE = 'notice'

  include Mongoid::Document
  field :date, type: DateTime
  field :metadata, type: String
  field :type, type: String
end
