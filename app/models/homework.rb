class Homework < ActiveRecord::Base
  STATUS_ACTIVE = 'active'
  STATUS_ARCHIVE = 'archive'
  belongs_to :group
end
