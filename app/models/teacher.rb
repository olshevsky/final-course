class Teacher < ActiveRecord::Base

  META_NAME = "Teacher"

  has_many :skills
  has_many :lessons
  has_many :salaries
  has_many :groups
  has_many :loads
  # belongs_to :user, as: :meta
  has_many :languages, through: :skills
  has_one :user, as: :meta, dependent: :destroy
  accepts_nested_attributes_for :user
end
