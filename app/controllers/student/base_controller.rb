class Student::BaseController < ApplicationController

  before_action 'check_rights', :authenticate_user!, :init_service

  layout 'student_layout'

  def index
  end

  private
  def check_rights
    if user_signed_in?
      if current_user.meta.class != Student
        access_denied
      else
        account_archived if current_user.status == User::STATUS_ARCHIVE
        account_blocked if current_user.status == User::STATUS_BLOCKED
      end
    end
  end

  def init_service
    @student_service = Services::Student::StudentService.new(current_user.meta)
  end
end
