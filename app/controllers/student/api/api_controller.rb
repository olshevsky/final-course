class Student::Api::ApiController < Student::BaseController
  before_action :init_service
  respond_to :json
  def index
    start_date = params[:start] || Date.today.beginning_of_week(:monday)
    end_date = params[:end] || Date.today.end_of_week(:monday)
    @events = @student_service.get_calendar_events start_date, end_date
  end
end
