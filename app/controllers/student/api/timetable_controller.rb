class Student::Api::TimetableController < Student::BaseController
  before_action
  def index
    render :json => @student_service.get_lessons_grouped
  end
end
