class Student::Api::HomeworkController < Student::BaseController
  def group
    @group = Group.find params[:id]
  end

  def show
    @homework = Homework.find params[:id]
  end
end
