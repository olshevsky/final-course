class Teacher::Api::GroupController < Teacher::BaseController
  def all
    @groups = Group.where(:id => current_user.meta.lessons.map {|lesson| lesson.group_id})
  end

  def show
    @group_service = Services::Teacher::GroupService.new
    @group = Group.find_by_id(params[:id])
    if @group
      @group
      @events = Event.where(meta: Group::META_NAME, meta_id: @group.id)
    else
      render :json => {}, :status => :not_found
    end
  end

  def add
    @group = Group.create(group_params)
  end

  private
  def group_params

  end
end
