class Teacher::Api::TimetableController < Teacher::BaseController
  def index
    @teacher_service = Services::Teacher::TeacherService.new(current_user.meta)
    render json: @teacher_service.get_lessons_grouped
  end
end
