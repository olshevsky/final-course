class Teacher::Api::HomeworkController < Teacher::BaseController
  skip_before_filter :verify_authenticity_token, :only => [:create, :update, :delete]
  def create
    @homework = Homework.new(homework_params)
    @homework.date = params[:date].in_time_zone
    if @homework.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def group
    @group = Group.find params[:id]
  end

  def show
    @homework = Homework.find params[:id]
  end

  def delete
    @homework = Homework.find params[:id]
    if @homework.delete
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def update
    @homework = Homework.find params[:id]
    if @homework.update!(homework_params)
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  protected
  def homework_params
    params.require(:homework).permit(:title, :description, :group_id, :date, :status)
  end
end
