class Teacher::Api::AccountingController < Teacher::BaseController
  def all
    @salaries = current_user.meta.salaries
  end
end
