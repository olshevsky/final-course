class Teacher::Api::ApiController < Teacher::BaseController
  before_action :init_service
  respond_to :json
  def index
    start_date = params[:start] || Date.today.beginning_of_week(:monday)
    end_date = params[:end] || Date.today.end_of_week(:monday)
    @teacher_service.get_lessons_grouped
    @events = @teacher_service.get_calendar_events start_date, end_date
  end

  private
  def init_service
    @teacher_service = Services::Teacher::TeacherService.new(current_user.meta)
  end
end
