class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
  def access_denied
    render :file => 'public/403.html', :status => :not_acceptable, :layout => false
  end

  def not_found
    render :file => 'public/404.html', :status => :not_found, :layout => false
  end

  def account_blocked
    render :file => 'public/account_blocked.html', :status => :not_found, :layout => false
  end

  def account_archived
    render :file => 'public/account_archived.html', :status => :not_found, :layout => false
  end

  protected
  def mongolog
    return Mongolog.new
  end

  before_filter :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit :username, :email, :password, :password_confirmation
    end
  end
end
