class Admin::Api::ApiController < Admin::BaseController
  before_action :init_service
  respond_to :json
  def index
    @events = Event.where(date: Time.now).where(type: Event::LESSON_TYPE)
  end

  def calendar
    start_date = params[:start] || Date.today.beginning_of_week(:monday)
    end_date = params[:end] || Date.today.end_of_week(:monday)
    @events = @admin_service.get_calendar_events start_date, end_date
  end
end
