class Admin::Api::GroupDistributionsController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  def create
    @distibution = Distribution.new distribution_params
    @distibution.status = Distribution::STATUS_ACTIVE
    if @distibution.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def destroy
    @distibution = Distribution.find params[:id]
    @distibution.delete
    render :json => {
        :success => true
    }
  end

  private
  def distribution_params
    params.require(:distribution).permit(:group_id, :student_id)
  end
end
