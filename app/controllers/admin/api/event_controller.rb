class Admin::Api::EventController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  def show
    if Event.find(params[:id])
      @event = Event.find(params[:id])
    else
      render :json => {success: false}
    end
  end

  def destroy
    if Event.find(params[:id]).remove
      render :json => {:success => true}
    else
      render :json => {:success => false}
    end
  end

  def create
    @event = Event.new(event_params)
    if @event.type == Event::LESSON_TYPE
      @event.meta = Group::META_NAME
      @event.event_class = Event::CLASS_ACTIVE
      @event.status = Event::STATUS_ACTIVE
      @event.meta_id = params[:event][:group][:id]

      # (s.sessionDateTime >= :beginDate AND s.sessionDateTime <= :endDate)
      # OR (s.sessionDateTime BETWEEN :beginDate AND :endDate)
      # OR (DATEADD(s.sessionDateTime, INTERVAL s.duration MINUTE) BETWEEN :beginDate AND :endDate)
      # OR (:beginDate BETWEEN s.sessionDateTime AND DATEADD(s.sessionDateTime, INTERVAL s.duration MINUTE))
      # OR (:endDate BETWEEN s.sessionDateTime AND DATEADD(s.sessionDateTime, INTERVAL s.duration MINUTE))
      @event_exists = Event
        .where(:meta_id => @event.meta_id)
        .where(:meta => Group::META_NAME)
        .where(:type => Event::LESSON_TYPE)
        .or(
            {:start => @event.start..@event.end},
            {:end => @event.start..@event.end},
          )
        .where(:date => @event.date)
      .first
    else
      if @event.type == Event::EVENT_TYPE
        @event.meta = Admin::META_NAME
        @event.event_class = Event::CLASS_EVENT
        @event.status = Event::STATUS_ACTIVE
        @event.meta_id = current_user.meta.id
      end
    end
    if @event.type == Event::LESSON_TYPE
      if !@event_exists.nil?
        if Group.find(@event.meta_id).teacher_id == Group.find(@event_exists.meta_id).teacher_id
          return render :json => {
              :success => false,
              :error => 'Lesson already exists!'
          }
        else
          if @event.save!
            return render :json => {
                :success => true
            }
          else
            return render :json => {
                :success => false,
                :message => 'Validation error'
            }
          end
        end
      else
        if @event.save!
          return render :json => {
              :success => true
          }
        else
          return render :json => {
              :success => false,
              :message => 'Validation error'
          }
        end
      end
    else
      if @event.type == Event::EVENT_TYPE
        if @event.save!
          return render :json => {
              :success => true
          }
        else
          return render :json => {
              :success => false,
              :message => 'Validation error'
          }
        end
      end
    end
  end

  def archive
    @event = Event.find(params[:id])
    @event.status = Event::STATUS_ARCHIVE
    @event.event_class = Event::CLASS_ARCHIVE

    @event.save

    render :json => {
        :success => true
    }
  end

  def reject
    @event = Event.find(params[:id])
    @event.status = Event::STATUS_REJECTED
    @event.event_class = Event::CLASS_CANCELED

    @event.save

    render :json => {
        :success => true
    }
  end

  def activate
    @event = Event.find(params[:id])
    @event.status = Event::STATUS_ACTIVE
    @event.event_class = Event::CLASS_ACTIVE

    @event.save

    render :json => {
        :success => true
    }
  end

  private
  def event_params
    if params[:event][:type] == Event::LESSON_TYPE
      params.require(:event).permit(:title, :group, :teacher, :start, :end, :type, :date)
    else
      if params[:event][:type] == Event::EVENT_TYPE
        params.require(:event).permit(:title, :description, :date, :start, :end, :status, :type)
      end
    end
  end
end
