class Admin::Api::HomeworkController < Admin::BaseController
  def group
    @group = Group.find params[:id]
  end

  def show
    @homework = Homework.find params[:id]
  end
end
