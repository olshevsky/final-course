class Admin::Api::TeacherController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  def index
    @teachers = Teacher.all
  end

  def create
    @teacher = User.new user_params

    if @teacher.valid?
      @teacher.meta = Teacher.new teacher_params
      @teacher.status = User::STATUS_ACTIVE
      @teacher.save
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def destroy
    @teacher = Teacher.find params[:id]
    @teacher.delete
    render :json => {
        :success => true
    }
  end

  def form_data
    @data = User.all.select(:login, :email)
  end

  def archive
    @teacher = Teacher.find params[:id]
    @teacher.user.status = User::STATUS_ARCHIVE
    if @teacher.user.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def activate
    @teacher = Teacher.find params[:id]
    @teacher.user.status = User::STATUS_ACTIVE
    if @teacher.user.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  private
  def teacher_params
    params.require(:teacher).permit(:inn, :passport, user_params: {:user => [:login, :name, :email, :password]})
  end

  def user_params
    params.require(:teacher).require(:user).permit(:login, :name, :email, :password)
  end
end
