class Admin::Api::LanguagesController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  def index
    render :json => {
        :languages => Language.all,
        :success => true
    }
  end

  def create
    @language = Language.new(language_params)
    if @language.save
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def destroy
    @language = Language.find params[:id]
    @language.delete
    render :json => {
        :success => true
    }
  end

  def update
    @language = Language.find params[:id]
    if @language.update!(language_params)
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  private
  def language_params
    params.require(:language).permit(:title)
  end
end
