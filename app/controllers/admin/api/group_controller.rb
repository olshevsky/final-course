class Admin::Api::GroupController < Admin::BaseController
  skip_before_filter :verify_authenticity_token

  def index
    @groups = Group.all
    @teachers = Teacher.all
    @languages = Language.all
  end

  def show
    @group = Group.find_by_id(params[:id])
    @not_students = Student.where.not(id: @group.students)
    if @group
      @group
      @events = Event.where(meta: Group::META_NAME, meta_id: @group.id)
    else
      render :json => {}, :status => :not_found
    end
  end

  def create
    @group = Group.new(group_params)
    @group.status = Group::STATUS_ACTIVE
    if @group.save!
      @load = Load.new ({
        :group => @group,
        :teacher => @group.teacher,
        :rate => params[:group][:rate]
      })
      @load.save
      render :json => {
          :success => true
      }
      @group.students = Student.where(:id => params[:group][:students])
      @group.save
    else
      render :json => {
          :success => false
      }
    end
  end

  def form_data
    @groups = Group.all
    @teachers = Teacher.all
    @languages = Language.all
    @courses = Course.all
    @students = Student.all
  end

  def destroy
    @group = Group.find params[:id]
    @group.delete
    render :json => {
        :success => true
    }
  end

  def archive
    @group = Group.find params[:id]
    @group.status = Group::STATUS_ARCHIVE
    if @group.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def activate
    @group = Group.find params[:id]
    @group.status = Group::STATUS_ACTIVE
    if @group.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  private
  def group_params
    params.require(:group).permit(:title, :course_id, :teacher_id, :language_id)
  end
end
