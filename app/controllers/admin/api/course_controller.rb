class Admin::Api::CourseController < Admin::BaseController
  skip_before_filter :verify_authenticity_token
  def index
    render :json => {
        :courses => Course.all,
        :success => true
    }
  end

  def destroy
    @course = Course.find params[:id]
    @course.delete
    render :json => {
        :success => true
    }
  end

  def create
    @course = Course.new course_params
    if @course.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def update
    @course = Course.find params[:id]
    if @course.update!(course_params)
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def archive
    @event = Course.find(params[:id])
    @event.status = Course::STATUS_ARCHIVE

    if @event.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def reject
    @event = Course.find(params[:id])
    @event.status = Course::STATUS_REJECTED

    if @event.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  def activate
    @event = Course.find(params[:id])
    @event.status = Course::STATUS_ACTIVE

    if @event.save!
      render :json => {
          :success => true
      }
    else
      render :json => {
          :success => false
      }
    end
  end

  private
  def course_params
      params.require(:course).permit(:title, :description, :startDate, :endDate, :enrollTo, :price, :status)
  end
end
