class Admin::Api::ProfileController < ApplicationController
  def index
    @user = {
        :meta => current_user.meta,
        :profile => current_user
    }

    render :json => @user
  end
end
