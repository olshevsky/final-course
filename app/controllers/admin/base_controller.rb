class Admin::BaseController < ApplicationController

  before_action 'check_rights', :authenticate_user!

  layout 'admin_layout'

  def index

  end

  private
  def check_rights
    if user_signed_in?
      if current_user.meta.class != Admin
        access_denied
      else
        account_archived if current_user.status == User::STATUS_ARCHIVE
        account_blocked if current_user.status == User::STATUS_BLOCKED
      end
    end
  end

  def init_service
    @admin_service = Services::Admin::AdminService.new(current_user.meta)
  end
end
