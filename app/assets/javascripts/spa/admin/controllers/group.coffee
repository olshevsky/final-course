angular.module('schoolSpa_admin').controller("groupShowCtrl",  ($scope, $http, $modal, homeworkService, groupService, $rootScope, userService, API_ROUTER, $routeParams) ->
  $scope.user = userService.getUser()
  $scope.distribution = {}
  $scope.distribution.group_id = $routeParams.id
  $scope.group = {}

  @init = () ->
    $http.get(API_ROUTER.group + $routeParams.id).success (data) =>
      $scope.group = data
      $scope.group.homeworks = homeworkService.getForGroup $routeParams.id
  @init()

  $rootScope.$on('update:group:view', (event, id) =>
    if $scope.group.id is id
      console.log "Reinit"
      @init()
  )

  $rootScope.$on('user:init', () ->
    $scope.user = userService.getUser()
  )

  $scope.RemoveStudent = (id) ->
    groupService.removeStudent $scope.group.id, id

  $scope.AddStudent = (distribution) ->
    groupService.addStudent distribution, $scope.group.id

  $rootScope.$on('group:receive_homework', () =>
    $scope.group.homeworks = homeworkService.getHomeworks()
  )
  $scope.showHomeworkInfo = (id) ->
    modalInstance = $modal.open {
      templateUrl: '/assets/spa/admin/partials/homework_info_modal.html',
      controller: 'homeworkInfoModalCtrl',
      resolve: {
        id: () ->
          id
      }
    }
)