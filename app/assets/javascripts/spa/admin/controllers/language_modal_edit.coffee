angular.module('schoolSpa_admin').controller("languageModalEditCtrl", ($scope, languageService, $modalInstance, $rootScope, $location, language) ->
  $scope.language = language;

  @init = () ->
    console.log "Inited"
  @init()

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')

  $scope.saveLanguage = (language) ->
    languageService.update(language)
    $modalInstance.dismiss('cancel')

)