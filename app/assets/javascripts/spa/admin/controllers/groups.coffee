angular.module('schoolSpa_admin').controller("groupsCtrl", ($scope, $modal, $http, GROUP_CONSTANTS, $rootScope, groupService, API_ROUTER)->
  $scope.data = groupService.getAll() || {}
  $scope.groups = $scope.data.groups
  $scope.GROUP_CONSTANTS = GROUP_CONSTANTS

  $rootScope.$on('groups:update', ()->
    $scope.data = groupService.getAll()
    $scope.groups = $scope.data.groups
  )

  $scope.ArchiveGroup = (group) ->
    if confirm "Are you sure"
      groupService.archive group.id
  $scope.RemoveGroup = (group) ->
    if confirm "Are you sure"
      groupService.delete group.id
  $scope.ActivateGroup = (group) ->
    if confirm "Are you sure"
      groupService.activate group.id
  $scope.addNewGroup = () ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/group_modal_form.html',
      controller: 'addGroupModalCtrl',
      resolve:
        event: () =>
          []
    )
)