angular.module('schoolSpa_admin').controller("courseModalEditCtrl", ($scope, course, courseService, $modalInstance, $rootScope, $location) ->
  $scope.course = course;
  $scope.statuses = [
    'archive'
    'active'
    'rejected'
  ]

  @init = () ->
    console.log "Inited"
  @init()

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')

  $scope.saveCourse = (language) ->
    courseService.update(language)
    $modalInstance.dismiss('cancel')

  $scope.openFrom = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.openedFrom = true
  $scope.openTo = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.openedTo = true
  $scope.openEnroll = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.openedEnroll = true

)