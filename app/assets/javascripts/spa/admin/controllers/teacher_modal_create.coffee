angular.module('schoolSpa_admin').controller("teacherModalCreateCtrl", ($scope, teacherService, $modalInstance, $rootScope, $location, API_ROUTER) ->
  $scope.formData = teacherService.getFormData() || {}
  $scope.emails = $scope.formData.emails || []
  $scope.logins = $scope.formData.logins || []
  $scope.statuses = [
    'archive'
    'active'
    'rejected'
  ]

  @init = () ->
   console.log "Inited"
  @init()
  $rootScope.$on('teacher:form_data:received', (event, data) ->
    $scope.formData = data
    $scope.emails = $scope.formData.emails
    $scope.logins = $scope.formData.logins
  )

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')

  $scope.SaveTeacher = (teacher) ->
    if $scope.teacher_form.$valid
      teacherService.save(teacher)
      $modalInstance.dismiss('cancel')
    else
      alert "Check your data!"

)