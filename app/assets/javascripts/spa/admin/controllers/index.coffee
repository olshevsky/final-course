angular.module('schoolSpa_admin').controller("indexCtrl",  ($scope, eventService, $modal, EVENT_CONSTANTS, $compile, $http, $rootScope, baseService, API_ROUTER) ->
  $scope.user = baseService.getUser();
  $scope.EVENT_CONSTANTS = EVENT_CONSTANTS
  $scope.eventsToday = baseService.getAllLessons();

  $rootScope.$on('base:init', () ->
    $scope.user = baseService.getUser()
    $scope.eventsToday = baseService.getAllLessons()
  )

  $scope.eventSources = {
    url: API_ROUTER.calendar
  }

  $scope.showDesc = (event, jsEvent, view) ->


  $scope.alertOnEventClick = ( event, allDay, jsEvent, view ) ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/event_modal.html',
      controller: 'eventModalCtrl',
      resolve:
        event: () =>
          event.id
    )
  $scope.createEvent = () ->
    modalInstance = $modal.open
      templateUrl: '/assets/spa/admin/partials/create_event_modal.html'
      controller: 'createEventModalCtrl'
      resolve: {}

  $scope.eventRender = ( event, element, view ) ->
    element.attr(
      'tooltip': event.title
      'tooltip-append-to-body': true
    );
    $compile(element)($scope);

  $scope.uiConfig = {
    calendar:{
      height: 400,
      firstDay: 1,
      defaultView: 'agendaWeek',
      slotDuration: '00:30:00',
      allDaySlot: false,
      editable: false,
      header:{
        left: 'agendaDay agendaWeek month',
        center: 'title',
        right: 'today prev,next'
      },
      eventMouseover: $scope.showDesc,
      eventClick: $scope.alertOnEventClick
      eventRender: $scope.eventRender
    }
  };

  $scope.RemoveEvent = (event)->
    if confirm "Are you sure?"
      eventService.delete(event.id)

  $scope.RejectEvent = (event)->
    if confirm "Are you sure?"
      eventService.reject(event.id)

  $scope.ArchiveEvent = (event)->
    if confirm "Are you sure?"
      eventService.archive(event.id)

  $scope.ActivateEvent = (event)->
    if confirm "Are you sure?"
      eventService.activate(event.id)

  $rootScope.$on 'events:needs:reload', ()->
    $scope.adminEventsCalendar.fullCalendar('refetchEvents');
)