angular.module('schoolSpa_admin').controller("adminNavigation", ($scope, $http, $rootScope, $location) ->
  $scope.isActive = (viewLocation) ->
    return viewLocation is $location.path()
)