angular.module('schoolSpa_admin').controller("teachersCtrl", ($scope, $modal, $http, GROUP_CONSTANTS, $rootScope, teacherService, API_ROUTER)->
  $scope.data = teacherService.getAll() || {}
  $scope.teachers = $scope.data.teachers
  $scope.GROUP_CONSTANTS = GROUP_CONSTANTS

  $rootScope.$on('teachers:update', ()->
    $scope.data = teacherService.getAll()
    $scope.teachers = $scope.data.teachers
  )

  $scope.ArchiveTeacher = (group) ->
    if confirm "Are you sure"
      teacherService.archive group.id
  $scope.RemoveTeacher = (group) ->
    if confirm "Are you sure"
      teacherService.delete group.id
  $scope.ActivateTeacher = (group) ->
    if confirm "Are you sure"
      teacherService.activate group.id
  $scope.addNewTeacher = () ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/teacher_modal_create.html'
      controller: 'teacherModalCreateCtrl'
      size: 'lg'
      resolve:
        event: () =>
          []
    )
)