angular.module('schoolSpa_admin').controller("addGroupModalCtrl", ($scope, $modalInstance, $http, GROUP_CONSTANTS, $rootScope, groupService, API_ROUTER)->
  $scope.data = groupService.getFormData()
  $scope.group = {}
  $scope.group.students = []

  $rootScope.$on('form_data:received', (event, data)->
    $scope.data = data
    console.log $scope.data
  )

  $scope.addStudentField = ()=>
    $scope.group.students.push {}

  $scope.RemoveStudent = (index) =>
    $scope.group.students.splice index, 1

  $scope.SaveGroup = (group) ->
    groupService.save group
    $modalInstance.dismiss('cancel')

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')
)