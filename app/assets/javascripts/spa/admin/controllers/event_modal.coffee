angular.module('schoolSpa_admin').controller("eventModalCtrl", ($scope, eventService, EVENT_CONSTANTS, $http, API_ROUTER, $modalInstance, $rootScope, $location, event) ->
  $scope.EVENT_CONSTANTS = EVENT_CONSTANTS
  console.log(EVENT_CONSTANTS)

  @init = () ->
    $http.get(API_ROUTER.events + event).success (data) =>
      if(data.success)
        $scope.event = data
        console.log($scope.event)
      else
        alert "Something wrong!"
  @init()

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')

  $scope.RemoveEvent = (event)->
    if confirm "Are you sure?"
      eventService.delete(event.id)
      $modalInstance.dismiss('cancel')

  $scope.RejectEvent = (event)->
    if confirm "Are you sure?"
      eventService.reject(event.id)
      $modalInstance.dismiss('cancel')

  $scope.ArchiveEvent = (event)->
    if confirm "Are you sure?"
      eventService.archive(event.id)
      $modalInstance.dismiss('cancel')

  $scope.ActivateEvent = (event)->
    if confirm "Are you sure?"
      eventService.activate(event.id)
      $modalInstance.dismiss('cancel')
)