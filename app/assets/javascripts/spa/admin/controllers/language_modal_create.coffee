angular.module('schoolSpa_admin').controller("languageModalCreateCtrl", ($scope, languageService, $modalInstance, $rootScope, $location) ->
  $scope.language = {};
  $scope.lessonsData = {};

  @init = () ->
    console.log "Inited"
  @init()

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')

  $scope.saveLanguage = (language) ->
    languageService.save(language)
    $modalInstance.dismiss('cancel')

)