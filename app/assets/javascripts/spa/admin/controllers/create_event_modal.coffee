angular.module('schoolSpa_admin').controller("createEventModalCtrl", ($scope, eventService, EVENT_CONSTANTS, $http, API_ROUTER, $modalInstance, $rootScope, $location) ->
  $scope.EVENT_CONSTANTS = EVENT_CONSTANTS
  $scope.event = {};
  $scope.lessonsData = {};

  @init = () ->
    $scope.event.type = $scope.EVENT_CONSTANTS.LESSON_TYPE
    $http.get(API_ROUTER.lesson_data).success (data) =>
      if(data.success)
        $scope.lessonsData = data
        console.log($scope.lessonsData)
      else
        alert "Something wrong!"
  @init()

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')

  $scope.saveEvent = (event) ->
    if $scope.event_form.$valid
      eventService.save(event)
      $modalInstance.dismiss('cancel')
    else
      alert "Check your data"

  $scope.openTo = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()

    $scope.openedTo = true
)