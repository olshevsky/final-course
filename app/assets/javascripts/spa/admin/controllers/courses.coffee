angular.module('schoolSpa_admin').controller("coursesCtrl", ($scope, $modal, $http, EVENT_CONSTANTS, $rootScope, courseService, API_ROUTER)->
  $scope.data = courseService.getAll() || {}
  $scope.EVENT_CONSTANTS = EVENT_CONSTANTS
  $scope.courses =  $scope.data.courses

  $rootScope.$on('courses:update', ()->
    $scope.data = courseService.getAll()
    $scope.courses = $scope.data.courses
  )

  $scope.RemoveCourse = (group) ->
    if confirm "Are you sure"
      courseService.delete group.id

  $scope.RejectCourse = (group)->
    if confirm "Are you sure?"
      courseService.reject(group.id)

  $scope.ArchiveCourse = (group)->
    if confirm "Are you sure?"
      courseService.archive(group.id)

  $scope.ActivateCourse = (group)->
    if confirm "Are you sure?"
      courseService.activate(group.id)

  $scope.addNewCourse = () ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/course_modal_create.html',
      controller: 'courseModalCreateCtrl',
      resolve:
        event: () =>
          []
    )
  $scope.EditCourse = (course) ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/course_modal_create.html',
      controller: 'courseModalEditCtrl',
      resolve:
        course: () =>
          course
    )
)