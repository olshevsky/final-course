angular.module('schoolSpa_admin').controller("languagesCtrl", ($scope, $modal, $http, GROUP_CONSTANTS, $rootScope, languageService, API_ROUTER)->
  $scope.data = languageService.getAll() || {}
  $scope.languages =  $scope.data.languages

  $rootScope.$on('languages:update', ()->
    $scope.data = languageService.getAll()
    $scope.languages = $scope.data.languages
  )

  $scope.RemoveLanguage = (group) ->
    if confirm "Are you sure"
      languageService.delete group.id
  $scope.addNewLanguage = () ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/language_modal_create.html',
      controller: 'languageModalCreateCtrl',
      resolve:
        event: () =>
          []
    )
  $scope.EditLanguage = (language) ->
    modalInstance = $modal.open(
      templateUrl: '/assets/spa/admin/partials/language_modal_create.html',
      controller: 'languageModalEditCtrl',
      resolve:
        language: () =>
          language
    )
)