angular.module('schoolSpa_admin').service("teacherService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @teachers = []

  service = {}

  @get = () =>
    $http.get(API_ROUTER.teacher)
    .success((data, stauts, headers, config) =>
      @teachers = data
      $rootScope.$broadcast('teachers:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.save = (teacher) ->
    $http(
      method: 'POST'
      url: API_ROUTER.teacher
      data: {teacher: teacher}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('teachers:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )
  service.getFormData = () =>
    console.log API_ROUTER.teacher_form_data
    $http({
      method: 'GET',
      url: API_ROUTER.teacher_form_data
    })
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('teacher:form_data:received', data);
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  $rootScope.$on('teachers:needs:update', ()=>
    @get()
  )

  service.getAll = () =>
    return @teachers

  service.archive = (id) ->
    $http.get(API_ROUTER.teacher + id + API_ROUTER.event_archive)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('teachers:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.activate = (id) ->
    $http.get(API_ROUTER.teacher + id + API_ROUTER.event_activate)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('teachers:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.delete = (id) ->
    $http.delete(API_ROUTER.teacher + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('teachers:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  return service
])