angular.module('schoolSpa_admin').service("courseService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @courses = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.course
    })
    .success((data, stauts, headers, config) =>
      @courses = data
      $rootScope.$broadcast('courses:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.save = (course) ->
    $http(
      method: 'POST'
      url: API_ROUTER.course
      data: {course: course}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('courses:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )
  service.update = (course) ->
    $http(
      method: 'PATCH'
      url: API_ROUTER.course + course.id
      data: {course: course}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('courses:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.delete = (id) ->
    $http.delete(API_ROUTER.course + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('courses:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.reject = (id) ->
    $http.get(API_ROUTER.course + id + API_ROUTER.event_reject)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('courses:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.archive = (id) ->
    console.log "Archive!"
    $http.get(API_ROUTER.course + id + API_ROUTER.event_archive)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('courses:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.activate = (id) ->
    $http.get(API_ROUTER.course + id + API_ROUTER.event_activate)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('courses:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  @get()

  $rootScope.$on('courses:needs:update', ()=>
    @get()
  )

  service.getAll = () =>
    return @courses

  return service
])