angular.module('schoolSpa_admin').service("baseService", ['$http', '$rootScope', 'API_ROUTER', 'userService',  ($http, $rootScope, API_ROUTER, userService) ->
  @lessons = []
  @user = {}

  service = {}

  $rootScope.$on('user:init', () =>
    $rootScope.$broadcast('base:init');
    @user = userService.getUser()
  );

  @getLessons = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.base
    })
    .success((data, stauts, headers, config) =>
      @lessons = data
      $rootScope.$broadcast('base:init');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @getLessons()

  $rootScope.$on 'events:needs:reload', (event, data)=>
    @getLessons()

  service.getAllLessons = () =>
    return @lessons

  service.getUser = () =>
    return @user

  return service
])