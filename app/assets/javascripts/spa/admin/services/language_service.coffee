angular.module('schoolSpa_admin').service("languageService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @groups = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.language
    })
    .success((data, stauts, headers, config) =>
      @groups = data
      $rootScope.$broadcast('languages:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.save = (language) ->
    $http(
      method: 'POST'
      url: API_ROUTER.language
      data: {language: language}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('languages:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.update = (language) ->
    $http(
      method: 'PATCH'
      url: API_ROUTER.language + language.id
      data: {language: language}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('languages:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  $rootScope.$on('languages:needs:update', ()=>
    @get()
  )

  service.getAll = () =>
    return @groups

  service.delete = (id) ->
    $http.delete(API_ROUTER.language + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('languages:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  return service
])