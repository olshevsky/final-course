angular.module('schoolSpa_admin').service("groupService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @groups = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.group
    })
    .success((data, stauts, headers, config) =>
      @groups = data
      $rootScope.$broadcast('groups:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.save = (group) ->
    $http(
      method: 'POST'
      url: API_ROUTER.group
      data: {group: group}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('groups:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  $rootScope.$on('groups:needs:update', ()=>
    @get()
  )

  service.getAll = () =>
    return @groups

  service.getFormData = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.group_form_data
    })
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('form_data:received', data);
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.archive = (id) ->
    $http.get(API_ROUTER.group + id + API_ROUTER.event_archive)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('groups:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.removeStudent = (group_id, id) ->
    $http.delete(API_ROUTER.group + group_id + API_ROUTER.distribution_prefix + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('update:group:view', group_id)
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.addStudent = (distribution, group_id) ->
    $http.post(API_ROUTER.group + group_id + API_ROUTER.distribution_prefix, {distribution: distribution})
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('update:group:view', group_id)
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.activate = (id) ->
    $http.get(API_ROUTER.group + id + API_ROUTER.event_activate)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('groups:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.delete = (id) ->
    $http.delete(API_ROUTER.group + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('groups:needs:update')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  return service
])