angular.module('schoolSpa_admin').service("eventService", ['$http', '$rootScope', 'API_ROUTER', '$location',  ($http, $rootScope, API_ROUTER, $location) ->
  service = {}

  service.getTodayEvents = ()->


  service.save = (event) ->
    @result = no
    $http.post(API_ROUTER.events, {event: event})
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('events:needs:reload')
      else
        alert "Something wrong." + data.error
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.delete = (id) ->
    $http.delete(API_ROUTER.events + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('events:needs:reload')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.update = (id, event) ->
    $http(
      method: 'PATCH',
      url: API_ROUTER.events + id,
      data: angular.toJson(event)
    )
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('events:needs:reload')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.reject = (id) ->
    $http.get(API_ROUTER.events + id + API_ROUTER.event_reject)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('events:needs:reload')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.archive = (id) ->
    $http.get(API_ROUTER.events + id + API_ROUTER.event_archive)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('events:needs:reload')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.activate = (id) ->
    $http.get(API_ROUTER.events + id + API_ROUTER.event_activate)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('events:needs:reload')
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );


  service.getTodayEvents = () ->
    return homeworksStack

  return service
])