angular.module('schoolSpa_admin').service("lessonService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @groups = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.lesson
    })
    .success((data, stauts, headers, config) =>
      @groups = data
      $rootScope.$broadcast('lessons:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  service.save = (group) ->
    $http(
      method: 'POST'
      url: API_ROUTER.group
      data: {group: group}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('groups:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )
  service.save = (group) ->
    $http(
      method: 'POST'
      url: API_ROUTER.group
      data: {group: group}
    )
    .success((data, stauts, headers, config) =>
      $rootScope.$broadcast('groups:needs:update');
      if(data.success)
        alert "Saved!"
      else
        alert "something wrong! Try again..."
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  $rootScope.$on('groups:needs:update', ()=>
    @get()
  )

  service.getAll = () =>
    return @groups

  return service
])