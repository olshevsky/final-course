angular.module('schoolSpa_admin').service("userService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @user = {};
  service = {};
  @getCurrentUser = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.profile
    })
    .success((data, stauts, headers, config) =>
      @user.profile = data.profile
      @user.meta = data.meta
      $rootScope.$broadcast('user:init');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )
  @getCurrentUser()

  service.getUser = () =>
    return @user

  return service
])