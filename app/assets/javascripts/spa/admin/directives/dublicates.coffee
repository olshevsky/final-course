angular.module('schoolSpa_admin').directive('duplicate', () ->
  {
  restrict: 'A',
  require: 'ngModel',
  link: (scope, elm, attrs, ctrl, viewValue)->
    ctrl.$parsers.unshift((viewValue)->
      duplicate = scope[attrs.duplicate]
      if (duplicate.indexOf(viewValue) isnt -1)
        ctrl.$setValidity('duplicate', false)
        return undefined
      else
        ctrl.$setValidity('duplicate', true)
        return viewValue
    )
  }
)