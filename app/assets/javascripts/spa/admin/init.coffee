app = angular.module 'schoolSpa_admin', ['ngRoute', 'pascalprecht.translate', 'ngSanitize', 'ui.select', "solo.table", "ui.calendar" , "angular-capitalize-filter", "ui.bootstrap"]
app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider.when '/',
    templateUrl: '/assets/spa/admin/partials/index.html',
    controller: 'indexCtrl'
  $routeProvider.when '/groups',
    templateUrl: '/assets/spa/admin/partials/groups.html',
    controller: 'groupsCtrl'
  $routeProvider.when '/timetable',
    templateUrl: '/assets/spa/admin/partials/timetable.html',
    controller: 'timetableCtrl'
  $routeProvider.when '/pays',
    templateUrl: '/assets/spa/admin/partials/pays.html',
    controller: 'payCtrl'
  $routeProvider.when '/group/:id',
    templateUrl: '/assets/spa/admin/partials/group.html',
    controller: 'groupShowCtrl'
  $routeProvider.when '/students',
    templateUrl: '/assets/spa/admin/partials/students.html',
    controller: 'stuntsCtrl'
  $routeProvider.when '/teachers',
    templateUrl: '/assets/spa/admin/partials/teachers.html',
    controller: 'teachersCtrl'
  $routeProvider.when '/events',
    templateUrl: '/assets/spa/admin/partials/events.html',
    controller: 'eventsCtrl'
  $routeProvider.when '/salary',
    templateUrl: '/assets/spa/admin/partials/salary.html',
    controller: 'salaryCtrl'
  $routeProvider.when '/languages',
    templateUrl: '/assets/spa/admin/partials/languages.html',
    controller: 'languagesCtrl'
    $routeProvider.when '/courses',
      templateUrl: '/assets/spa/admin/partials/courses.html',
      controller: 'coursesCtrl'

]
angular.module('schoolSpa_admin').constant('DAYS',
  monday: 'monday'
  tuesday: 'tuesday'
  wednesday: 'wednesday'
  thursday: 'thursday'
  friday: 'friday'
  saturday: 'saturday'
  sunday: 'sunday'
)
app.constant('API_ROUTER', {
  base: "/admin/api/"
  profile: "/admin/api/profile",
  group: "/admin/api/groups/",
  group_form_data: "/admin/api/group/form_data/",
  teacher_form_data: "/admin/api/teacher/form_data/"
  lesson: "/admin/api/lessons/",
  language: "/admin/api/languages/"
  course: "/admin/api/courses/"
  lesson_data: '/admin/api/data/lesson/'
  salary: "/admin/api/pays/",
  events: "/admin/api/events/"
  distribution_prefix: '/distributions/'
  homework: "/admin/api/homework/",
  teacher: '/admin/api/teachers/'
  report: "/admin/api/report/",
  new: "new/",
  edit: "edit/",
  delete: "delete/",
  group_prefix: "group/",
  calendar: "/admin/api/calendar/"
  event_reject: '/reject'
  event_archive: '/archive'
  event_activate: '/activate'
});

app.constant('EVENT_CONSTANTS',
  STATUS_CANCELED: 'canceled'
  STATUS_ACTIVE: 'active'
  STATUS_FINISHED_SUCCESS: 'finished_success'
  STATUS_REJECTED: 'rejected'
  STATUS_ARCHIVE: 'archive'

  LESSON_TYPE: 'lesson'
  EVENT_TYPE: 'event'

  CLASS_ACTIVE: 'event-active'
  CLASS_FINISHED_SUCCESS: 'event-active'
  CLASS_CANCELED: 'event-canceled'
  CLASS_EVENT: 'personal-event'
  CLASS_REJECTED: 'event-canceled'
);

app.constant('GROUP_CONSTANTS',
  STATUS_FINISHED: "finished"
  STATUS_ACTIVE: "active"
  STATUS_BLOCKED: "blocked"
  STATUS_ARCHIVE: "archive"

  CLASS_FINISHED: "label-success"
  CLASS_ACTIVE: "label-primary"
  CLASS_BLOCKED: "label-danger"
  CLASS_ARCHIVE: "label-warning"
);

app.config(['$translateProvider', ($translateProvider) ->
  $translateProvider.translations('en', {
    'Timetable': 'Timetable'
  });

  $translateProvider.translations('ru',
    'Timetable': 'Расписание'
    'Wednesday': 'Среда'
  );
  $translateProvider.preferredLanguage('en');
])

angular.module('schoolSpa_admin').constant('angularMomentConfig', {
  preprocess: 'utc',
  timezone: 'Europe/Kiev'
});
app.value('current_user', {});
$(document).on 'page:load', ->
  $('[ng-app]').each ->
    module = $(this).attr('ng-app')
    angular.bootstrap(this, [module])