angular.module('schoolSpa_student').controller("studentNavigation", ($scope, $http, $rootScope, $location) ->
  $scope.isActive = (viewLocation) ->
    return viewLocation is $location.path()
)