angular.module('schoolSpa_student').controller("groupsCtrl", ($scope, $http, $rootScope, groupService) ->
  $scope.groups = groupService.getAll()

  $rootScope.$on('groups:update', ()->
    $scope.groups = groupService.getAll()
  )
)