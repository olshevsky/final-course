angular.module('schoolSpa_student').controller("timetableCtrl", ($scope, DAYS, $http, $rootScope, lessonsService) ->
  $scope.days = lessonsService.getAll()
  $scope.DAYS = DAYS

  $rootScope.$on('timetable:update', ()->
    $scope.days = lessonsService.getAll()
    console.log($scope.days)
  )

)