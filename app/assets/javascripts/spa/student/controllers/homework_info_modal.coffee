angular.module('schoolSpa_student').controller("homeworkInfoModalCtrl",  ($scope, $modalInstance, $http, $rootScope, API_ROUTER, $routeParams, groupService, homeworkService, $location, id) ->
  $scope.groups = groupService.getAll()
  console.log(id)

  @init = () ->
    $http.get(API_ROUTER.homework + id).success (data) =>
      if(data.success)
        $scope.homework = data
      else
        alert "Something wrong!"
  @init()

  $scope.statuses = [
    'archive'
    'active'
  ]

  $rootScope.$on('groups:update', ()->
    $scope.groups = groupService.getAll()
  )

  $scope.openCalendar = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.openedFrom = true

  $scope.updateHomework = ()->
    if !$scope.homework_form.$valid
      alert "Check your data"
    else
      homeworkService.update id, $scope.homework
      $modalInstance.dismiss('cancel')

  $scope.removeHomework = (id) ->
    homeworkService.delete id, $scope.homework
    $modalInstance.dismiss('cancel')

  $scope.cancel = () ->
    $modalInstance.dismiss('cancel')
)