angular.module('schoolSpa_student').controller("groupShowCtrl",  ($scope, $http, $modal, homeworkService, $rootScope, userService, API_ROUTER, $routeParams) ->
  $scope.user = userService.getUser()
  $scope.group = {}

  @init = () ->
    $http.get(API_ROUTER.group + $routeParams.id).success (data) =>
      $scope.group = data
      $scope.group.homeworks = homeworkService.getForGroup $routeParams.id
  @init()

  $rootScope.$on('user:init', () ->
    $scope.user = userService.getUser()
  )

  $rootScope.$on('group:receive_homework', () =>
    $scope.group.homeworks = homeworkService.getHomeworks()
  )
  $scope.showHomeworkInfo = (id) ->
    modalInstance = $modal.open {
      templateUrl: '/assets/spa/student/partials/homework_info_modal.html',
      controller: 'homeworkInfoModalCtrl',
      resolve: {
        id: () ->
          id
      }
    }
)