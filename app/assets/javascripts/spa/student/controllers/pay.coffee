angular.module('schoolSpa_student').controller("payCtrl",  ($scope, $http, $rootScope, payService, API_ROUTER) ->
  $scope.pays = payService.getAll() || []

  $rootScope.$on('pays:update', ()->
    $scope.pays = payService.getAll()
  )

  $scope.openFrom = ($event) ->
    console.log 'From'
    $event.preventDefault()
    $event.stopPropagation()

    $scope.openedFrom = true

    $scope.openTo = ($event) ->
      console.log 'To'
      $event.preventDefault()
      $event.stopPropagation()

      $scope.openedTo = true
)