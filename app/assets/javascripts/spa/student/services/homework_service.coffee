angular.module('schoolSpa_student').service("homeworkService", ['$http', '$rootScope', 'API_ROUTER', '$location',  ($http, $rootScope, API_ROUTER, $location) ->
  homeworksStack = [];
  currentHomework = {};
  service = {}

  service.getForGroup = (groupId) =>
    $http.get(API_ROUTER.homework + API_ROUTER.group_prefix + groupId, {})
    .success((data, status, headers, config) =>
      if data.success
        homeworksStack = data.homeworks;
      else
        alert "?!"
      $rootScope.$broadcast('group:receive_homework', homeworksStack)
    )
    .error((data, status, headers, config) =>
      alert "Something wrong! Check your connection."
    )

  service.getHomeworks = () ->
    return homeworksStack

  $rootScope.$on('homeworks:needs:reload', (event, id)->
    service.getForGroup(id)
  )
  return service
])