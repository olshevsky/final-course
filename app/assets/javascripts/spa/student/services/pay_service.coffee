angular.module('schoolSpa_student').service("payService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @pays = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.salary
    })
    .success((data, stauts, headers, config) =>
      @pays = data
      $rootScope.$broadcast('pays:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  service.getAll = () =>
    return @pays

  return service
])