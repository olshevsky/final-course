app = angular.module 'schoolSpa_student', ['ngRoute', 'pascalprecht.translate', "solo.table", "ui.calendar" , "angular-capitalize-filter", "ui.bootstrap"]
app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider.when '/',
    templateUrl: '/assets/spa/student/partials/index.html',
    controller: 'indexCtrl'
  $routeProvider.when '/groups',
    templateUrl: '/assets/spa/student/partials/groups.html',
    controller: 'groupsCtrl'
  $routeProvider.when '/timetable',
    templateUrl: '/assets/spa/student/partials/timetable.html',
    controller: 'timetableCtrl'
  $routeProvider.when '/pays',
    templateUrl: '/assets/spa/student/partials/pays.html',
    controller: 'payCtrl'
  $routeProvider.when '/group/:id',
    templateUrl: '/assets/spa/student/partials/group.html',
    controller: 'groupShowCtrl'

]
angular.module('schoolSpa_student').constant('DAYS',
  monday: 'monday'
  tuesday: 'tuesday'
  wednesday: 'wednesday'
  thursday: 'thursday'
  friday: 'friday'
  saturday: 'saturday'
  sunday: 'sunday'
)
app.constant('API_ROUTER', {
  base: "/student/api/"
  profile: "/student/api/profile",
  group: "/student/api/group/",
  lesson: "/student/api/lesson/",
  salary: "/student/api/pays/",
  homework: "/student/api/homework/",
  report: "/student/api/report/",
  new: "new/",
  edit: "edit/",
  delete: "delete/",
  group_prefix: "group/"
});

app.config(['$translateProvider', ($translateProvider) ->
  $translateProvider.translations('en', {
      'Timetable': 'Timetable'
    });

  $translateProvider.translations('ru',
    'Timetable': 'Расписание'
    'Wednesday': 'Среда'
  );
  $translateProvider.preferredLanguage('en');
])

angular.module('schoolSpa_student').constant('angularMomentConfig', {
  preprocess: 'utc',
  timezone: 'Europe/Kiev'
});
app.value('current_user', {});
$(document).on 'page:load', ->
  $('[ng-app]').each ->
    module = $(this).attr('ng-app')
    angular.bootstrap(this, [module])