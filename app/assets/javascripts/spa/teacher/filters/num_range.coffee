angular.module('schoolSpa_teacher').filter('numRange', ()->
  (items, from, to)->
    if (!from || from is undefined ) && (!to || to is undefined)
      items
    else
      rangeFilter = (el, idx, array)->
        if (from && from isnt undefined) && (!to || to is undefined)
          return (parseFloat(el.sum) >= parseFloat(from))
        else if (to && to isnt undefined) && (!from || from is undefined )
          return (parseFloat(el.sum) <= parseFloat(to))
        else if (from && from isnt undefined ) && (to && to isnt undefined )
          return (parseFloat(el.sum) >= parseFloat(from)) && (parseFloat(el.sum) <= parseFloat(to));
      items.filter(rangeFilter)
)