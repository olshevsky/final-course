angular.module('schoolSpa_teacher').service("lessonsService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @lessons = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.lesson
    })
    .success((data, stauts, headers, config) =>
      @lessons = data
      $rootScope.$broadcast('timetable:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  service.getAll = () =>
    return @lessons

  return service
])