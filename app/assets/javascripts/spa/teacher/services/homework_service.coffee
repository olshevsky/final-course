angular.module('schoolSpa_teacher').service("homeworkService", ['$http', '$rootScope', 'API_ROUTER', '$location',  ($http, $rootScope, API_ROUTER, $location) ->
  homeworksStack = [];
  currentHomework = {};
  service = {}

  service.save = (homework, $scope) ->
    @result = no
    $http.post(API_ROUTER.homework + API_ROUTER.new, homework)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('homeworks:needs:reload', homework.group_id)
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.delete = (id, homework) ->
    $http.delete(API_ROUTER.homework + API_ROUTER.delete + id)
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('homeworks:needs:reload', homework.group_id)
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.update = (id ,homework) ->
    $http(
      method: 'PATCH',
      url: API_ROUTER.homework + id,
      data: angular.toJson(homework)
    )
    .success((data, status, headers, config) =>
      if data.success
        $rootScope.$broadcast('homeworks:needs:reload', homework.group_id)
      else
        alert "Something wrong. Try again!"
    )
    .error((data, status, headers, config) =>
      @result = data.success
      alert "Something wrong. Try again!"
    );

  service.getForGroup = (groupId) =>
    $http.get(API_ROUTER.homework + API_ROUTER.group_prefix + groupId, {})
    .success((data, status, headers, config) =>
      if data.success
        homeworksStack = data.homeworks;
      else
        alert "?!"
      $rootScope.$broadcast('group:receive_homework', homeworksStack)
    )
    .error((data, status, headers, config) =>
      alert "Something wrong! Check your connection."
    )

  service.getHomeworks = () ->
    return homeworksStack

  $rootScope.$on('homeworks:needs:reload', (event, id)->
    service.getForGroup(id)
  )
  return service
])