angular.module('schoolSpa_teacher').service("salaryService", ['$http', '$rootScope', 'API_ROUTER',  ($http, $rootScope, API_ROUTER) ->
  @salaries = []

  service = {}

  @get = () =>
    $http({
      method: 'GET',
      url: API_ROUTER.salary
    })
    .success((data, stauts, headers, config) =>
      @salaries = data
      $rootScope.$broadcast('salary:update');
    )
    .error((data, status, headers, config) =>
      alert "Something wrong..."
    )

  @get()

  service.getAll = () =>
    return @salaries

  return service
])