angular.module('schoolSpa_teacher').controller("timetableCtrl", ($scope, $http, $rootScope, lessonsService) ->
  $scope.days = lessonsService.getAll()

  $rootScope.$on('timetable:update', ()->
    $scope.days = lessonsService.getAll()
    console.log($scope.days)
  )

)