angular.module('schoolSpa_teacher').controller("salaryCtrl",  ($scope, $http, $rootScope, salaryService, API_ROUTER) ->
  $scope.salaries = salaryService.getAll() || []

  $rootScope.$on('salary:update', ()->
    $scope.salaries = salaryService.getAll()
  )

  $scope.openFrom = ($event) ->
    console.log 'From'
    $event.preventDefault()
    $event.stopPropagation()

    $scope.openedFrom = true

    $scope.openTo = ($event) ->
      console.log 'To'
      $event.preventDefault()
      $event.stopPropagation()

      $scope.openedTo = true
)