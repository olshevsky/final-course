angular.module('schoolSpa_teacher').controller("groupShowCtrl",  ($scope, $http, $modal, homeworkService, $rootScope, userService, API_ROUTER, $routeParams) ->
  $scope.user = userService.getUser()
  $scope.group = {}

  @init = () ->
    $http.get(API_ROUTER.group + $routeParams.id).success (data) =>
      $scope.group = data
      $scope.group.homeworks = homeworkService.getForGroup $routeParams.id
  @init()

  $rootScope.$on('user:init', () ->
    $scope.user = userService.getUser()
  )

  $rootScope.$on('group:receive_homework', () =>
    $scope.group.homeworks = homeworkService.getHomeworks()
  )

  $scope.openForm = ()->
    modalInstance = $modal.open {
      templateUrl: '/assets/spa/teacher/partials/new_homework_modal.html',
      controller: 'newHomeworkModalCtrl',
      size: 'lg',
      resolve: {}
    }
  $scope.showHomeworkInfo = (id) ->
    modalInstance = $modal.open {
      templateUrl: '/assets/spa/teacher/partials/homework_info_modal.html',
      controller: 'homeworkInfoModalCtrl',
      size: 'lg',
      resolve: {
        id: () ->
          id
      }
    }
)