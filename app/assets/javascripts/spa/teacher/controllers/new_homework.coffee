angular.module('schoolSpa_teacher').controller("newHomeworkCtrl",  ($scope, $http, $rootScope, API_ROUTER, $routeParams, groupService, homeworkService, $location) ->
  $scope.groups = groupService.getAll()

  $scope.statuses = [
    'archive'
    'active'
  ]

  $rootScope.$on('groups:update', ()->
    $scope.groups = groupService.getAll()
  )

  $scope.openCalendar = ($event) ->
    $event.preventDefault()
    $event.stopPropagation()
    $scope.openedFrom = true

  $scope.saveHomework = ()->
    if !$scope.homework_form.$valid
      alert "Check your data"
    else
      homeworkService.save $scope.homework, $scope
)