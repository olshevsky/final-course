json.array! @pays do |pay|
  json.(pay, :sum, :id)
  json.group_id pay.distribution.group_id
  json.group_title pay.distribution.group.title
  json.person_id pay.student.id
  json.date pay.created_at.in_time_zone
  json.person_name pay.student.user.name
end