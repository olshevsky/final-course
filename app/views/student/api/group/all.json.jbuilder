json.array! @groups do |group|
  json.(group, :title, :id)
  json.teacher group.teacher.user.name
  json.language group.language.title
  json.group_status group.status
  json.distribution_status current_user.meta.distributions.where(:group => group).first.status
  json.students group.students do |student|
    json.name student.user.name
    json.login student.user.login
  end
end