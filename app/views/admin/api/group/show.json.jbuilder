json.(@group, :id,:title, :status)
json.teacher @group.teacher.user.name
json.curator_id @group.teacher.id
json.language @group.language.title
json.lessons @group.lessons
json.group_id @group.id
json.course @group.course

json.students @group.students do |student|
  json.name student.user.name
  json.login student.user.login
  json.status student.distributions.where(:group => @group).first.status
  json.distribution student.distributions.where(:group => @group).first.id
end

json.not_students @not_students do |not_student|
  json.name not_student.user.name
  json.id not_student.id
end

json.homeworks @group.homeworks do |homework|
  json.date homework.date
  json.title homework.title
  json.id homework.id
  json.description homework.description
  json.status homework.status
end
json.events @events
