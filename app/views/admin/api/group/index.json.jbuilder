json.groups @groups do |group|
  json.(group, :id, :title, :created_at, :status)
  json.teacher_name group.teacher.user.name
  json.teacher_id group.teacher_id
  json.course_id group.course_id
  json.students_count group.students.length
  json.course_title group.course.title
  json.language group.language.title
end

json.languages @languages do |language|
  json.(language, :title, :id)
end

json.teachers @teachers do |teacher|
  json.teacher(teacher, :id)
  json.name teacher.user.name
end