json.array! @events do |event|

  start_time = Time.new(event.date.year, event.date.month, event.date.day, event.start.hour, event.start.min, 0)
  end_time = Time.new(event.date.year, event.date.month, event.date.day, event.end.hour, event.end.min, 0)

  if event.type == Event::EVENT_TYPE and event.meta == Student::META_NAME && event.meta_id == current_user.meta_id
    editable = true
  else
    editable = false
  end

  json.className event.event_class
  json.id event._id.to_s
  json.title event.title
  json.start start_time
  json.type event.type
  json.end end_time
  json.editable editable
  json.allDay false
end