json.events @events do |event|

  start_time = Time.new(event.date.year, event.date.month, event.date.day, event.start.hour, event.start.min, 0)
  end_time = Time.new(event.date.year, event.date.month, event.date.day, event.end.hour, event.end.min, 0)

  json.id event._id.to_s
  json.title event.title
  json.start start_time
  json.status event.status
  json.class_name event.event_class
  json.type event.type
  json.group_title Group.find(event.meta_id).title
  json.group_id Group.find(event.meta_id).id
  json.end end_time
  json.editable false
end