json.success true
json.teachers @teachers do |teacher|
  json.(teacher, :id)
  json.name teacher.user.name
  json.email teacher.user.email
  json.login teacher.user.login
  json.lastLogin teacher.user.last_sign_in_at
  json.createdAt teacher.user.created_at
  json.status teacher.user.status
end