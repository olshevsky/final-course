json.(@event, :type, :title, :description, :date, :start, :end, :status)
json.id @event.id.to_s
if @event.type == Event::LESSON_TYPE && @event.meta == Group::META_NAME
  group = Group.find(@event.meta_id)
  json.group_title group.title
  json.group_id group.id
end
json.success true