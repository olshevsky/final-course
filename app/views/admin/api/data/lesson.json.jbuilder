json.teachers @teachers do |teacher|
  json.(teacher, :id)
  json.name teacher.user.name
  json.loads teacher.loads.map {|load| load.group_id}
end

json.groups @groups do |group|
  json.(group, :title, :id)
end

json.success true