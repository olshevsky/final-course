json.success true
json.homeworks @group.homeworks do |homework|
  json.(homework, :title, :id, :description, :date, :status)
end