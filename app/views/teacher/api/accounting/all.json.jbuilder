json.array! @salaries do |salary|
  json.(salary, :sum, :id)
  json.person_id salary.teacher.id
  json.group_title salary.load.group.title
  json.group_id salary.load.group_id
  json.date salary.created_at.in_time_zone
  json.type salary.type
  json.lessons_num salary.num_lessons
  json.person_name salary.teacher.user.name
end